# ResNet18 model implementation
This is an implementation of the [ResNet network](https://arxiv.org/pdf/1512.03385v1) using tensorflow. 

We used the ResNet18 architecture that consists of nine convolution layers. 
It is trained on the cats vs dogs dataset.

### ResNet
ResNet models are residual networks used for image classification. 

They were developed in 2015 in order in order to circumvent the notorious problem of exploding/vanishing gradient. 

The basic idea is to use skip connection to mimic the identity function. This allows the signal to make its way through the whole network. 


